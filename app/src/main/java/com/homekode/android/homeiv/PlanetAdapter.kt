package com.homekode.android.homeiv

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.planet_item.view.*

/**
 * Created by ASUS on 06.12.2017.
 */
class PlanetAdapter(var data:Array<Planet>) : RecyclerView.Adapter<PlanetAdapter.UserViewHolder>() {
    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.img.setImageBitmap(data[position].img)
        holder.icon.setImageBitmap(data[position].live)
        holder.name.text = data[position].name
        holder.dist.text = data[position].distance

        holder.itemView.setOnClickListener(View.OnClickListener {
            var context = holder.itemView.context
            context.startActivity(Intent(context,Page::class.java).putExtra("URI",data[position].uri))
          Log.v("fgfg", "" + position)
        })

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            UserViewHolder(LayoutInflater.from(parent.context).
                    inflate(R.layout.planet_item,parent,false))

    override fun getItemCount() = data.size

    class UserViewHolder(var view: View): RecyclerView.ViewHolder(view) {
        var img = view.planet_img
        var icon = view.live_icon_img
        var name = view.name_txt_view
        var dist = view.dist_txt_view
}

}