package com.homekode.android.homeiv

import android.graphics.BitmapFactory
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var imgs = resources.obtainTypedArray(R.array.planetsImg)
        var data = Array(9, {i -> Planet(
                resources.getStringArray(R.array.planets)[i],
                BitmapFactory.decodeResource(resources,imgs.getResourceId(i,R.drawable.mercury)),
                resources.getStringArray(R.array.sundistance)[i] + if(i != 8) " " + getString(R.string.mil_km) else "",
                BitmapFactory.decodeResource(resources,if (i == 2 || i == 8) R.drawable.live else R.drawable.voidimg),
                Uri.parse( resources.getStringArray(R.array.planetsUri)[i]))})

        var resycler_planet = resycler_planet
        resycler_planet.layoutManager = GridLayoutManager(applicationContext,2)
        resycler_planet.adapter = PlanetAdapter(data)
        resycler_planet.adapter.notifyDataSetChanged()

    }
}
