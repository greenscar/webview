package com.homekode.android.homeiv

import android.graphics.Bitmap
import android.net.Uri

/**
 * Created by ASUS on 06.12.2017.
 */
class Planet(var name: String, var img: Bitmap, var distance: String, var live: Bitmap, var uri: Uri)