package com.homekode.android.homeiv

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_page.*

class Page : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page)

        var webView = web_view
        webView.settings.javaScriptEnabled = true;
        var uri = intent.extras.get("URI")

        webView.webViewClient = object : WebViewClient() {
            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                Toast.makeText(applicationContext, getString(R.string.error) + description, Toast.LENGTH_SHORT).show()
            }

            override  fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }
        }
        webView.loadUrl(uri.toString());
    }

}